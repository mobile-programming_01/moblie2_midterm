import 'package:flutter/material.dart';

class DescrptionSection extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top:20),
      child: Column(
        children: [
          Text("This article is intended to provide a high-level "
              "overview of the architecture of Flutter, including the core "
              "principles and concepts that form its design.",
            style: TextStyle(
              fontSize: 16,
              color: Colors.black.withOpacity(0.7),
            ),
            textAlign: TextAlign.justify,
          ),
        ],
      ),
      
    );
  }

}